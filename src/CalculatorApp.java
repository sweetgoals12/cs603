import java.awt.*;
import java.awt.event.*;
import javax.swing.*;

public class CalculatorApp extends JFrame implements ActionListener {

    private final JTextField displayField;
    private final JButton[] numberButtons;
    private final JButton addButton;
    private final JButton subtractButton;
    private final JButton multiplyButton;
    private final JButton divideButton;
    private final JButton equalButton;
    private final JButton clearButton;
    
    private double num1, num2;
    private char operator;
    
    public CalculatorApp() {
        setTitle("LKA CalculatorApp");
        setSize(300, 400);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        
        // Create display field
        displayField = new JTextField(10);
        displayField.setEditable(false);
        
        // Create number buttons
        numberButtons = new JButton[10];
        for (int i = 0; i < 10; i++) {
            numberButtons[i] = new JButton(String.valueOf(i));
            numberButtons[i].addActionListener(this);
        }
        
        // Create operator buttons
        addButton = new JButton("+");
        subtractButton = new JButton("-");
        multiplyButton = new JButton("*");
        divideButton = new JButton("/");
        equalButton = new JButton("=");
        clearButton = new JButton("C");
        
        addButton.addActionListener(this);
        subtractButton.addActionListener(this);
        multiplyButton.addActionListener(this);
        divideButton.addActionListener(this);
        equalButton.addActionListener(this);
        clearButton.addActionListener(this);
        
        // Create layout using GridBagLayout
        setLayout(new GridBagLayout());
        GridBagConstraints gbc = new GridBagConstraints();
        
        gbc.gridx = 0;
        gbc.gridy = 0;
        gbc.gridwidth = 4;
        gbc.fill = GridBagConstraints.HORIZONTAL;
        add(displayField, gbc);
        
        gbc.gridwidth = 1;
        gbc.gridy = 1;
        for (int i = 1; i <= 9; i++) {
            gbc.gridx = (i - 1) % 3;
            gbc.gridy = 1 + (i - 1) / 3;
            add(numberButtons[i], gbc);
        }
        
        gbc.gridx = 1;
        gbc.gridy = 4;
        add(numberButtons[0], gbc);
        
        gbc.gridx = 3;
        gbc.gridy = 1;
        add(addButton, gbc);
        
        gbc.gridy = 2;
        add(subtractButton, gbc);
        
        gbc.gridy = 3;
        add(multiplyButton, gbc);
        
        gbc.gridy = 4;
        add(divideButton, gbc);
        
        gbc.gridx = 2;
        gbc.gridy = 4;
        add(equalButton, gbc);
        
        gbc.gridx = 0;
        gbc.gridy = 5;
        gbc.gridwidth = 4;
        add(clearButton, gbc);
    }
    
    @Override
    public void actionPerformed(ActionEvent e) {
        String buttonText = ((JButton) e.getSource()).getText();
        
        if (buttonText.matches("[0-9]")) {
            displayField.setText(displayField.getText() + buttonText);
        } else if (buttonText.equals("+") || buttonText.equals("-") || buttonText.equals("*") || buttonText.equals("/")) {
            num1 = Double.parseDouble(displayField.getText());
            operator = buttonText.charAt(0);
            displayField.setText("");
        } else if (buttonText.equals("=")) {
            num2 = Double.parseDouble(displayField.getText());
            double result = calculateResult(num1, num2, operator);
            displayField.setText(String.valueOf(result));
        } else if (buttonText.equals("C")) {
            displayField.setText("");
        }
    }
    
    private double calculateResult(double num1, double num2, char operator) {
        double result = 0.0;
        
        switch (operator) {
            case '+':
                result = num1 + num2;
                break;
            case '-':
                result = num1 - num2;
                break;
            case '*':
                result = num1 * num2;
                break;
            case '/':
                result = num1 / num2;
                break;
        }
        
        return result;
    }
    
    public static void main(String[] args) {
        SwingUtilities.invokeLater(() -> {
            CalculatorApp calculator = new CalculatorApp();
            calculator.setVisible(true);
        });
    }
}